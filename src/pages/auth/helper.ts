import * as yup from 'yup';
import {object, ObjectSchema} from "yup";

export type LoginForm = {
  username: string;
  password: string;
  remember: boolean;
};

const REQUIRED_ERROR = 'Обязательное поле';

export const authSchema: ObjectSchema<LoginForm> = object({
  username: yup
    .string()
    .required(REQUIRED_ERROR),
  password: yup
    .string()
    .min(8, 'Пароль должен содержать не менее 8 символов')
    .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[-+!@$%^&*()_]).{1,50}$/, 'Пароль должен содержать символы латинского алфавита в верхнем и нижнем регистре, а также минимум одну цифру и  один спецсимвол !@$%^&*()_-+')
    .required(REQUIRED_ERROR),
  remember: yup
    .boolean()
    .required(REQUIRED_ERROR),
});