import React from 'react';
import Header from "../../../../containers/Header";
import './style.css';

const AuthInfo = () => {
  return (
    <div className="auth-info">
      <Header />

      <div className="auth-info__content">
        <img src="/login-img.png" alt="login-image"/>
        <p className="auth-info__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet laboriosam magni molestiae. Aliquid animi aperiam blanditiis deleniti, dolor dolores dolorum error harum minima modi natus praesentium quas quidem sit vero.</p>
      </div>
    </div>
  );
};

export default AuthInfo;