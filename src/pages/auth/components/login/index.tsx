import React, {ChangeEvent, FormEvent, useState} from 'react';
import {Button, Checkbox, CustomFlowbiteTheme, Label, Spinner, TextInput} from "flowbite-react";
import {authSchema, LoginForm,} from "../../helper";
import {ValidationError} from "yup";
import {authUserApi} from "../../../../api/auth";
import './style.css';

const customSpinnerTheme: CustomFlowbiteTheme['spinner'] = {
  size: {
    xl: "w-20 h-20"
  }
};

const Login = () => {
  const [formState, setForm] = useState<LoginForm>({
    username: '',
    password: '',
    remember: false
  });
  const [validationErrors, setValidationErrors] = useState<Record<keyof LoginForm, string>>({
    username: '',
    password: '',
    remember: ''
  });
  const [loading, setLoading] = useState<boolean>(false);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setForm(prev => ({
      ...prev,
      [e.target.name]: e.target.value
    }));

    if (validationErrors[e.target.name as keyof LoginForm]) {
      setValidationErrors(prev => ({
        ...prev,
        [e.target.name]: ''
      }));
    }
  };

  const handleChangeCheckbox = (e: ChangeEvent<HTMLInputElement>) => {
    setForm(prev => ({
      ...prev,
      [e.target.name]: e.target.checked
    }));
  };

  const onSubmitForm = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    try {
      authSchema.validateSync(formState, {abortEarly: false});
      setLoading(true);
      authUserApi(formState)
        .then(data => {
          setLoading(false);
          alert('Вы прошли авторизацию)');
        })
        .catch(error => {
          // handle error
          console.error('error', error);
        });
    } catch (error) {
      (error as ValidationError).inner.forEach((err: ValidationError) => {
        setValidationErrors(prev => ({
          ...prev,
          [err.path as string]: err.message
        }));
      });
    }
  };

  if (loading) {
    return (
      <div className="text-center mt-10">
        <Spinner aria-label="loading" size="xl" theme={customSpinnerTheme} />
      </div>
    );
  }

  return (
    <div className="login">
      <h2 className="login__title">User login</h2>
      <p className="login__sub-title">Welcome back</p>

      <form className="login__form" onSubmit={onSubmitForm}>
        {/*{Пришлось заводить обертку над инпутом из-за helperText}*/}
        <div>
          <TextInput
            type="text"
            name="username"
            placeholder="Email or username"
            value={formState.username}
            onChange={handleChange}
            helperText={validationErrors.username}
            color={validationErrors.username && 'failure'}
          />
        </div>

        <div>
          <TextInput
            type="password"
            name="password"
            placeholder="Password"
            value={formState.password}
            onChange={handleChange}
            helperText={validationErrors.password}
            color={validationErrors.password && 'failure'}
          />
        </div>

        <div className="login__form-button">
          <div className="flex items-center gap-2">
            <Checkbox
              id="remember"
              name="remember"
              checked={formState.remember}
              onChange={handleChangeCheckbox}
            />
            <Label htmlFor="remember">Remember</Label>
          </div>

          <a href="#">Forgot password?</a>
        </div>

        <Button type="submit">Login</Button>
      </form>
    </div>
  );
};

export default Login;