import React from 'react';
import AuthInfo from "./components/auth-info";
import Login from "./components/login";
import './style.css';

const Auth = () => {
  return (
    <div className="auth">
      <AuthInfo />
      <Login />
    </div>
  );
};

export default Auth;