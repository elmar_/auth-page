import {LoginForm} from "../pages/auth/helper";
import {delay} from "../utils";

export const authUserApi = async (userData: LoginForm) => {
  await delay(2000);
  return userData;
};