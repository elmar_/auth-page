export const delay = async (ms: number): Promise<null> => new Promise(resolve => {
  setTimeout(() => resolve(null), ms);
});