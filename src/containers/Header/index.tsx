import React from 'react';
import {Button, Navbar} from "flowbite-react";

const Header = () => {
  return (
    <Navbar fluid rounded className="bg-inherit">
      <Navbar.Brand href="#">
        <img src="/logo192.png" className="mr-3 h-8 sm:h-10" alt="LOGO" />
        <span className="self-center whitespace-nowrap text-xl font-semibold text-white">LOGO</span>
      </Navbar.Brand>

      <div className="flex md:order-2">
        <Button>Create account</Button>
      </div>
    </Navbar>
  );
};

export default Header;