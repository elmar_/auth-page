import React from 'react';
import Auth from "./pages/auth";

function App() {
  return (
    <div className="App">
      <Auth />
    </div>
  );
}

export default App;
